/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include <opencv2/core/ocl.hpp>
#include <vector>

void sanityCheckMap(cv::Mat& mat, const float min, const float max, const float minValue, const float maxValue);

std::vector<cv::Point2f>::iterator getTopLeft(std::vector<cv::Point2f>& points);

std::vector<cv::Point2f>::iterator getBottomRight(std::vector<cv::Point2f>& points);

double distance(const cv::Point2f& a, const cv::Point2f& b);

bool findClosest(size_t& xIndex, size_t& yIndex, 
				const cv::Point2f point, const std::vector< std::vector<cv::Point2f> >& array, 
				float xTolerance, float yTolerance);

bool findClosest(size_t& index, const cv::Point2f point, const std::vector<cv::Point2f>& array, float xTolerance, float yTolerance);

void thompsonTauTest(const std::vector<float>& in, std::vector<size_t>& outliers, float criticalValue);

void interpolateMissingOnX(cv::Mat& mat);

void interpolateMissingOnY(cv::Mat& mat);

bool findDeadSpace(const cv::Mat& mat, cv::Rect& roi);

void removeSparseCols(cv::Mat& mat, float reject, bool front);

bool bilinearResize(const cv::Mat& inImage, cv::Mat& outImage, cv::Size size);



