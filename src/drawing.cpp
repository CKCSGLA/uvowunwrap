/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include "drawing.h"
#include <opencv2/viz/types.hpp>
#include <opencv2/imgproc.hpp>

void drawRows(cv::Mat& image, const std::vector< std::vector<cv::Point2f > >& rows)
{
	for(size_t i = 0; i < rows.size(); ++i)
	{
		for(size_t y = 0; y < rows[i].size(); ++y)
		{
			cv::circle(image, rows[i][y], 5, cv::viz::Color(128 * (i%3), 128 * ((i+1)%3), 128 * ((i+2)%3)));
		}
	}
}

void drawEllipses(cv::Mat& image, const std::vector<cv::RotatedRect>& ellipses )
{
	 for(const auto& ellipse : ellipses)cv::ellipse(image, ellipse, cv::viz::Color(128,128,128));
}
