/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include<argp.h>
#include<string>

struct Config
{
	char** commandsFiles = NULL;
	std::string output = "";
	std::string norm = "";
	std::string bg = "";
	std::string curve = "";
	bool harris = false;
	float minSize = 7; 
	unsigned int size = 50;
	bool verbose = false;
	bool quiet = false;
	bool interactive = false;
	bool simpleStich = false;
	float kFactor = 0;
};

const char *argp_program_version = "0.2";
const char *argp_program_bug_address = "<carl@uvos.xyz>";
static char doc[] = "Program to determine the lubricant thikness on a curved surface.\n\
Possible operations: apply create curve mkcurve mkboard show";
static char args_doc[] = "[OPERATION] IMAGE1 IMAGE2 ...";

static struct argp_option options[] = 
{
{"verbose",  'v', 0,      0,  "Enable verbose logging" },
{"quiet",  'q', 0,      0,  "Disable info messages" },
{"bg", 'b', "File Name",      0,  "background image file" },
{"normalize", 'n', "File Name",      0,  "image to use as a normalization source" },
{"min-size", 's', "Value",      0,  "Smallest feature accepted as a corner" },
{"size", 'x', "Value",      0,  "Output cell size" },
{"harris", 'r', 0,      0,  "Use harris to detect points" },
{"output", 'o', "File Name",      0,  "output file name" },
{"interactive", 'i', 0,      0,  "interactivly process multiple commands" },
{"simpe-stich", 'a', 0,      0,  "Use non blending sticher" },
{"curve", 'c', "File Name",      0,  "curve file name" },
{"kfactor", 'k',   "Value",      0,  "set the kfactor" },
{ 0 }
};


error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	Config* config = reinterpret_cast<Config*>(state->input);
	switch (key)
	{
		case 'v':
			config->verbose = true;
		break;
		case 'q':
			config->quiet = true;
		break;
		case 'b':
			config->bg.assign(arg);
		break;
		case 'n':
			config->norm.assign(arg);
		break;
		case 's':
			config->minSize=strtod(arg, nullptr);
		break;
		case 'r':
			config->harris = true;
		break;
		case 'x':
			config->size=atol(arg);
		break;
		case 'a':
			config->simpleStich = true;
		break;
		case 'i':
			config->interactive = true;
		break;
		case 'o':
			config->output.assign(arg);
		break;
		case 'c':
			config->curve.assign(arg);
		break;
		case 'k':
			config->kFactor=strtod(arg, nullptr);
		break;
		case ARGP_KEY_ARG:
			config->commandsFiles = &state->argv[state->next-1];
			state->next = state->argc;
			break;
		default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}
static struct argp argp = { options, parse_opt, args_doc, doc };



