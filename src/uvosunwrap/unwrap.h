/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include <opencv2/core/ocl.hpp>
#include <string>
#include "detectedpoint.h"

struct RemapedImage
{
	cv::Mat image;
	cv::Mat angle;
	cv::Point2i origin;
};

struct RemapMap
{
	cv::Mat xMat;
	cv::Mat yMat;
	cv::Point2i topLeftCoordinate;
	unsigned int outputCellSize;
};

bool saveRemapMap(const RemapMap& map, const std::string& fileName);

RemapMap loadRemapMap(const std::string& fileName);

bool createRemapMap(const cv::Mat& image, RemapMap& out, const std::vector<DetectedPoint>& points, bool verbose = false);

RemapedImage applyRemap(const cv::Mat& image, const RemapMap &map);

void applyKfactor(cv::Mat& image ,const cv::Mat& angleMat, float kFactor);

cv::Mat stich(std::vector<RemapedImage>& images, bool seamAdjust = false);
cv::Mat simpleStich(const std::vector<RemapedImage>& images);
