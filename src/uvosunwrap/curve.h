/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include <opencv2/core/ocl.hpp>
#include <assert.h>

float applyCurve(float in, cv::Mat curve)
{
	assert(curve.type() == CV_32FC1);
	assert(curve.rows == 2);

	float* keys   = curve.ptr<float>(0);
	float* values = curve.ptr<float>(1);
	
	if(in < keys[0])
		return values[0];
		

	for(int i = 1; i < curve.cols; ++i)
	{		
		if(in < keys[i])
		{
			double slope = (values[i] - (double)values[i-1])/(keys[i] - keys[i-1]);
			return (in-keys[i-1])*slope+values[i-1];
		}
	}
	return values[curve.cols-1];
}

void applyCurve(cv::Mat image, cv::Mat curve)
{
	assert(image.type() == CV_32FC1);
	assert(curve.type() == CV_32FC1);
	assert(curve.rows == 2);
	
	image.forEach<float>([&curve](float &p, const int * position){p = applyCurve(p, curve);});
}
