/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once

#include <string>
#include <opencv2/core/ocl.hpp>
#include "detectedpoint.h"

static constexpr unsigned int X_BOARD_SIZE = 20;
static constexpr unsigned int Y_BOARD_SIZE = 12;

void createCharucoBoard(unsigned int size, const std::string& fileName);

std::vector<DetectedPoint> detectCharucoPoints(cv::Mat image, bool verbose = true);
