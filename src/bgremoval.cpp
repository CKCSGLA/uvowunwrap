/**
* Lubricant Detecter
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include "uvosunwrap/bgremoval.h"
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/bgsegm.hpp>
#include "uvosunwrap/log.h"

bool createMask(const cv::Mat& in, cv::Mat& mask, const cv::Mat& bg)
{
	if(in.size != bg.size || in.type() != bg.type())
	{
		Log(Log::ERROR)<<"input image and backgournd image size and type needs to be the same";
		return false;
	}
	
	cv::Ptr<cv::BackgroundSubtractorMOG2> bgremv = cv::createBackgroundSubtractorMOG2(2,10,false);
	bgremv->apply(bg, mask, 1);
	bgremv->apply(in, mask, 0);
	cv::GaussianBlur(mask,mask,cv::Size(49,49), 15);
	cv::threshold(mask, mask, 70, 255, cv::THRESH_BINARY);
	return true;
}
