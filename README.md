UVOS libuvosunwrap and uvosunwrap_cli
==============

This is a library and cli application that implments algorithums for the UVOS lubricant thickness detector.  
license: GPL v3.0 or later

Building and Installing
----------

Requirements:

* cmake
* pkg-config
* openCV v3.0 or later

Build instructions:

1. run 'cmake .'
2. run 'make'

To install run 'make install' as root.

Unfortionatly no api for libuvosunwrap documentation is available at the moment, however the headers are quite understanable

uvosunwrap_cli usage
----------

To unwrap a object:

1. use 'uvosunwrap_cli mkboard -o $(filename.png)' to create a aruco board. 
2. Print this out and apply it to your obejct. 
3. take images of your shape from at least 2 sides
4. run 'uvosunwrap_cli create' for eatch
5. run 'uvosunwrap_cli apply -o $(filename.png) $(image1) $(image2) ...' 
6. to apply a callibration curve first create it with 'uvosunwrap_cli mkcurve' then run 'uvosunwrap_cli curve' on your image. 

for further actions look at 'uvosunwrap_cli --help'
